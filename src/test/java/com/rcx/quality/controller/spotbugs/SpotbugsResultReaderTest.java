package com.rcx.quality.controller.spotbugs;

import com.rcx.quality.model.QualityResult;
import com.rcx.quality.tools.StringTools;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class SpotbugsResultReaderTest {

  @Test
  public void testLoadSpotBugs() throws IOException {
    final String spotbugsFile = StringTools.convert(getClass().getResourceAsStream("/spotbugs/main.xml"));
    final QualityResult result = SpotbugsResultReader.loadSpotBugs(spotbugsFile);
    assertNotNull("Spotbugs loaded shouldn't be null", result);
    assertNotNull("Spotbugs results shouldn't be null", result.getResults());
    assertFalse("There is existing errors", result.getResults().isEmpty());
    assertEquals("There is 2 files with errors", 2, result.getResults().size());
  }

  @Test
  public void testLoadSpotBugsNoError() throws IOException {
    final String spotbugsFile = StringTools.convert(getClass().getResourceAsStream("/spotbugs/main_noerror.xml"));
    final QualityResult result = SpotbugsResultReader.loadSpotBugs(spotbugsFile);
    assertNotNull("Spotbugs loaded shouldn't be null", result);
    assertNotNull("Spotbugs results shouldn't be null", result.getResults());
    assertTrue("There is no existing errors", result.getResults().isEmpty());
  }

}