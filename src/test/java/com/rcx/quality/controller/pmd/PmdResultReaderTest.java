package com.rcx.quality.controller.pmd;

import com.rcx.quality.model.QualityResult;
import com.rcx.quality.tools.StringTools;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class PmdResultReaderTest {

  @Test
  public void testLoadPmd() throws IOException {
    final String pmdFile = StringTools.convert(getClass().getResourceAsStream("/pmd/main.xml"));
    final QualityResult result = PmdResultReader.loadPmd(pmdFile);
    assertNotNull("PMD loaded shouldn't be null", result);
    assertNotNull("PMD results shouldn't be null", result.getResults());
    assertFalse("There is existing errors", result.getResults().isEmpty());
    assertEquals("There is 2 files with errors", 2, result.getResults().size());
  }
}