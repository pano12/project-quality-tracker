package com.rcx.quality.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class QualityLineIssue {
  private int line;
  private String category;
  private String description;
  private QualitySeverity severity;
}
