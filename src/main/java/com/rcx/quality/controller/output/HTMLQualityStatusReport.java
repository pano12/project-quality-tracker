package com.rcx.quality.controller.output;

import com.rcx.quality.db.ProjectQualityStorage;
import com.rcx.quality.model.QualitySeverity;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HTMLQualityStatusReport {

  private static final String CANVAS_STYLE = "width:50%";

  private static final String QUALITY_FLAGS_TEMPLATE = "<!doctype html>\n" +
      "<html><head><title>%s</title>\n" +
      "\t<script src=\"https://www.chartjs.org/dist/2.7.3/Chart.js\"></script>\n" +
      "\t<script src=\"https://www.chartjs.org/samples/latest/utils.js\"></script>\n" +
      "</head>\n" +
      "\n" +
      "<body>\n" +
      "\t<div style=\"%s\">\n" +
      "\t\t<canvas id=\"canvas\"></canvas>\n" +
      "\t</div>\n" +
      "\t<br>\n" +
      "\t<script>\n" +
      "\n" +
      "\t\tvar color = Chart.helpers.color;\n" +

      "var config = {\n" +
      "\t\t\tdata: {\n" +
      "\t\t\t\tdatasets: [{\n" +
      "\t\t\t\t\tdata: [\n" +
      "\t\t\t\t\t\t%s\n" +
      "\t\t\t\t\t],\n" +
      "\t\t\t\t\tbackgroundColor: [\n" +
      "\t\t\t\t\t\tcolor(chartColors.red).alpha(0.5).rgbString(),\n" +
      "\t\t\t\t\t\tcolor(chartColors.orange).alpha(0.5).rgbString(),\n" +
      "\t\t\t\t\t\tcolor(chartColors.gray).alpha(0.5).rgbString()\n" +
      "\t\t\t\t\t]\n" +
      "\t\t\t\t}],\n" +
      "\t\t\t\tlabels: [\n" +
      "\t\t\t\t\t'Error',\n" +
      "\t\t\t\t\t'Warning',\n" +
      "\t\t\t\t\t'Info'\n" +
      "\t\t\t\t]\n" +
      "\t\t\t},\n" +
      "\t\t\toptions: {\n" +
      "\t\t\t\tstartAngle: 1.6,\n" +
      "\t\t\t\tresponsive: true,\n" +
      "\t\t\t\tlegend: {\n" +
      "\t\t\t\t\tposition: 'right'\n" +
      "\t\t\t\t},\n" +
      "\t\t\t\ttitle: {\n" +
      "\t\t\t\t\tdisplay: true,\n" +
      "\t\t\t\t\ttext: '%s'\n" +
      "\t\t\t\t},\n" +
      "\t\t\t\tscale: {\n" +
      "\t\t\t\t\tticks: {\n" +
      "\t\t\t\t\t\tbeginAtZero: true,\n" +
      "\t\t\t\t\t\tstepSize: 1\n" +
      "\t\t\t\t\t},\n" +
      "\t\t\t\t\treverse: false\n" +
      "\t\t\t\t},\n" +
      "\t\t\t\tanimation: {\n" +
      "\t\t\t\t\tanimateRotate: false,\n" +
      "\t\t\t\t\tanimateScale: true\n" +
      "\t\t\t\t}\n" +
      "\t\t\t}\n" +
      "\t\t};" +

      "\n" +
      "\t\twindow.onload = function() {\n" +
      "\t\t\tvar ctx = document.getElementById('canvas').getContext('2d');\n" +
      "\t\t\twindow.myPolarArea = Chart.PolarArea(ctx, config);\n" +
      "\n" +
      "\t\t};" +
      "</script>\n" +
      "</body>\n" +
      "\n" +
      "</html>";

  public static String generateHTMLReport(
      final String project,
      final Map<String, List<ProjectQualityStorage>> history) {

    final StringBuilder resultValues = new StringBuilder();

    int errorCount = 0;
    int warningCount = 0;
    int infoCount = 0;

    final Map<String, ProjectQualityStorage> lastValues = new HashMap<>();

    for (final Map.Entry<String, List<ProjectQualityStorage>> entry : history.entrySet()) {
      ProjectQualityStorage mapEntry = lastValues.get(entry.getKey());
        for (final ProjectQualityStorage s : entry.getValue()) {
          if (mapEntry == null || mapEntry.getMeasureTime() < s.getMeasureTime()) {
            mapEntry = lastValues.put(entry.getKey(), s);
          }
        }
    }

    for (final ProjectQualityStorage s : lastValues.values()) {
      errorCount += s.getErrorIssues();
      warningCount += s.getWarningIssues();
      infoCount += s.getInfoIssues();
    }

    resultValues.append(errorCount);
    resultValues.append(", ");
    resultValues.append(warningCount);
    resultValues.append(", ");
    resultValues.append(infoCount);

    return String
        .format(QUALITY_FLAGS_TEMPLATE, project + " Quality Status", CANVAS_STYLE,
            resultValues.toString(), project + " Quality Status");
  }

}
