package com.rcx.quality.controller.jacoco;

import com.rcx.quality.controller.QualityReader;
import com.rcx.quality.model.JacocoCoverageType;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JacocoResultReader extends QualityReader {

  private JacocoResultReader() {

  }

  public static double getLineCoverage(final String contents, final JacocoCoverageType type) {
    final Document jacocoDoc = loadDocument(contents);
    if (jacocoDoc == null) {
      return -1;
    }

    NodeList jacocoResults = jacocoDoc.getElementsByTagName("report");
    if (jacocoResults.getLength() == 1) {
      jacocoResults = jacocoResults.item(0).getChildNodes();
    }

    for (int i = 0; i < jacocoResults.getLength(); i++) {
      final Node node = jacocoResults.item(i);
      if ("counter".equals(node.getNodeName()) &&
          type.name().equalsIgnoreCase(node.getAttributes().getNamedItem("type").getNodeValue())) {
        // Correct coverage type found
        int missed = Integer.parseInt(node.getAttributes().getNamedItem("missed").getNodeValue());
        int covered = Integer.parseInt(node.getAttributes().getNamedItem("covered").getNodeValue());
        return (double) covered / (missed + covered);
      }
    }

    return 0;
  }
}
