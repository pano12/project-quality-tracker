docker volume create qualityVolume
docker create --rm --name projectQuality -v d:\docker_test\volumes\quality:/quality -v qualityVolume:/data pano12/project-quality:1.0-SNAPSHOT -c=/data/checkstyle.xml -p=/data/pmd.xml -s=/data/spotbugs.xml -j=/data/jacoco.xml -g=Pano -n="Project Quality"
docker cp build\reports\checkstyle\main.xml projectQuality:/data/checkstyle.xml
docker cp build\reports\pmd\main.xml projectQuality:/data/pmd.xml
docker cp build\reports\spotbugs\main.xml projectQuality:/data/spotbugs.xml
docker cp build\reports\jacoco\main.xml projectQuality:/data/jacoco.xml
docker start projectQuality -a
docker volume rm qualityVolume