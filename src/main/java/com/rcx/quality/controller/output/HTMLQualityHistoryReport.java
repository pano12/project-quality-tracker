package com.rcx.quality.controller.output;

import com.rcx.quality.db.ProjectQualityStorage;
import com.rcx.quality.model.QualitySeverity;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class HTMLQualityHistoryReport {

  private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  private static final String PROJECT_NAME_DATA_FORMAT = "{\n"
      + "label: '%s',\n"
      + "fill: false,\n"
      + "data: [\n"
      + "%s\n"
      + "],"
      + "backgroundColor: '#' + colorTable[%d],\n"
      + "borderColor: '#' + colorTable[%d],\n"
      + "pointStyle: 'rectRot',\n"
      + "pointRadius: 5,\n"
      + "pointBorderColor: 'rgb(0, 0, 0)'\n"
      + "}\n";

  private static final String PROJECT_DATA_SINGLE_FORMAT = "{ x: %s, y : %s }";

  private static final String QUALITY_FLAGS_TEMPLATE = "<!doctype html>\n" +
      "<html><head><title>%s</title>\n" +
      "\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js\"></script>\n" +
      "\t<script src=\"https://www.chartjs.org/dist/2.7.3/Chart.js\"></script>\n" +
      "\t<script src=\"https://www.chartjs.org/samples/latest/utils.js\"></script>\n" +
      "\t<script src=\"https://codepen.io/anon/pen/aWapBE.js\"></script>\n" +
      "</head>\n" +
      "\n" +
      "<body>\n" +
      "\t<div>\n" +
      "\t\t<canvas id=\"canvas\"></canvas>\n" +
      "\t</div>\n" +
      "\t<br>\n" +
      "\t<script>\n" +
      "\t\tvar timeFormat = 'YYYY/MM/DD HH:mm';\n" +
      "\n" +
      "\t\tvar color = Chart.helpers.color;\n" +
      "\t\tvar colorTable = palette('tol-dv', %d);\n" +
      "\t\tvar config = {\n" +
      "\t\t\ttype: 'line',\n" +
      "\t\t\tdata: {\n" +
      "\t\t\t\tdatasets: [" +
      "%s" +
      "]},\n" +
      "\t\t\toptions: {\n" +
      "\t\t\t\ttitle: {\n" +
      "\t\t\t\t\ttext: '%s',\n" +
      "\t\t\t\t\tdisplay: true\n" +
      "\t\t\t\t},\n" +
      "\t\t\t\tlegend: { position: 'bottom' },\n" +
      "\t\t\t\tscales: {\n" +
      "\t\t\t\t\txAxes: [{\n" +
      "\t\t\t\t\t\ttype: 'time',\n" +
      "\t\t\t\t\t\ttime: {\n" +
      "\t\t\t\t\t\t\tunit: 'day',\n" +
      "\t\t\t\t\t\t\ttooltipFormat: 'll HH:mm'\n" +
      "\t\t\t\t\t\t},\n" +
      "\t\t\t\t\t\tscaleLabel: {\n" +
      "\t\t\t\t\t\t\tdisplay: true,\n" +
      "\t\t\t\t\t\t\tlabelString: 'Execution Date'\n" +
      "\t\t\t\t\t\t}\n" +
      "\t\t\t\t\t}],\n" +
      "\t\t\t\t\tyAxes: [{\n" +
      "\t\t\t\t\t\tscaleLabel: {\n" +
      "\t\t\t\t\t\t\tdisplay: true,\n" +
      "\t\t\t\t\t\t\tlabelString: '%s'\n" +
      "\t\t\t\t\t\t}\n" +
      "\t\t\t\t\t}]\n" +
      "\t\t\t\t},\n" +
      "\t\t\t}\n" +
      "\t\t};\n" +
      "\n" +
      "\t\twindow.onload = function() {\n" +
      "\t\t\tvar ctx = document.getElementById('canvas').getContext('2d');\n" +
      "\t\t\twindow.myLine = new Chart(ctx, config);\n" +
      "\n" +
      "\t\t};" +
      "</script>\n" +
      "</body>\n" +
      "\n" +
      "</html>";

  public static String generateHTMLReport(
      final String project,
      final Map<String, List<ProjectQualityStorage>> history) {

    final StringBuilder resultValues = new StringBuilder();

    StringBuilder tmp;
    int count = 0;
    for (final Map.Entry<String, List<ProjectQualityStorage>> entry : history.entrySet()) {
      for (final QualitySeverity severity : QualitySeverity.values()) {
        tmp = new StringBuilder();
        for (final ProjectQualityStorage s : entry.getValue()) {
          tmp.append(',');
          tmp.append(
              String.format(
                  PROJECT_DATA_SINGLE_FORMAT,
                  String.valueOf(s.getMeasureTime()),
                  String.valueOf(
                      severity == QualitySeverity.ERROR ? s.getErrorIssues() :
                          severity == QualitySeverity.WARNING ? s.getWarningIssues() :
                              s.getInfoIssues()),
                  formatDate(s.getMeasureTime()) + " [" + entry.getKey() + " (" + severity.name() +
                      ")]"
              ));
          tmp.append('\n');
        }
        resultValues.append(',');
        resultValues.append(String.format(
            PROJECT_NAME_DATA_FORMAT,
            severity.name() + " " + entry.getKey(),
            tmp.substring(1),
            count,
            count
        ));
        count++;
      }
    }
    return String
        .format(QUALITY_FLAGS_TEMPLATE, project + " Quality Dashboard", count,
            resultValues.substring(1), project + " Quality Dashboard",
            "Quality Issues count");
  }

  private static String formatDate(final long date) {
    return FORMATTER.format(new Date(date));
  }
}
