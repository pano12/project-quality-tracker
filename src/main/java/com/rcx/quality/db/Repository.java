package com.rcx.quality.db;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface Repository extends CrudRepository<ProjectQualityStorage, Long> {

  List<ProjectQualityStorage> findAllByProjectGroupOrderByMeasureTimeAsc(
      final String projectGroup);
}
