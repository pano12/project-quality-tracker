package com.rcx.quality.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

@Getter
public class QualityResult {
  private Map<String, List<QualityLineIssue>> results = new HashMap<>();
}
