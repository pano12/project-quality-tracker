package com.rcx.quality.controller.spotbugs;

import com.rcx.quality.controller.QualityReader;
import com.rcx.quality.model.QualityLineIssue;
import com.rcx.quality.model.QualityResult;
import com.rcx.quality.model.QualitySeverity;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SpotbugsResultReader extends QualityReader {

  private SpotbugsResultReader() {

  }

  public static QualityResult loadSpotBugs(final String contents) {
    final Document spotbugs = loadDocument(contents);
    final QualityResult result = new QualityResult();
    if (spotbugs == null) {
      return result;
    }

    final NodeList bugList = spotbugs.getElementsByTagName("BugInstance");
    if (bugList == null || bugList.getLength() == 0) {
      return result;
    }
    List<QualityLineIssue> lineIssues;
    for (int i = 0; i < bugList.getLength(); i++) {
      final Node node = bugList.item(i);
      final String category = node.getAttributes().getNamedItem("category").getNodeValue() + "/" +
          node.getAttributes().getNamedItem("type").getNodeValue();
      final int priority =
          Integer.parseInt(node.getAttributes().getNamedItem("priority").getNodeValue());
      QualitySeverity qualitySeverity;
      if (priority == 2) {
        qualitySeverity = QualitySeverity.WARNING;
      } else if (priority > 2) {
        qualitySeverity = QualitySeverity.INFO;
      } else {
        qualitySeverity = QualitySeverity.ERROR;
      }
      final NodeList bugInfos = node.getChildNodes();
      for (int j = 0; j < bugInfos.getLength(); j++) {
        final Node details = bugInfos.item(j);
        if ("SourceLine".equals(details.getNodeName())) {
          final String file = details.getAttributes().getNamedItem("sourcepath").getNodeValue();
          final int lineNumber =
              Integer.parseInt(details.getAttributes().getNamedItem("start").getNodeValue());

          if (result.getResults().containsKey(file)) {
            lineIssues = result.getResults().get(file);
          } else {
            lineIssues = new ArrayList<>();
            result.getResults().put(file, lineIssues);
          }
          lineIssues.add(new QualityLineIssue(lineNumber, category, "", qualitySeverity));
        }
      }
    }

    return result;
  }
}
