package com.rcx.quality.controller;

import java.io.IOException;
import java.io.StringReader;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class QualityReader {

  protected static Document loadDocument(final String contents) {
    try {
      final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
      builderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
//      builderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
      builderFactory.setValidating(false);
      builderFactory.setNamespaceAware(true);
      builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
      builderFactory.setFeature("http://xml.org/sax/features/validation", false);
      builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
      builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

      final DocumentBuilder builder = builderFactory.newDocumentBuilder();
      return builder.parse(new InputSource(new StringReader(contents)));

    } catch (final ParserConfigurationException | SAXException | IOException e) {
      //
      e.printStackTrace();
    }
    return null;
  }

  protected static NodeList loadChildNodes(final String contents) {
    final Document doc = loadDocument(contents);
    if (doc != null) {
      NodeList nodeList = doc.getChildNodes();
      // Parent node = checkstyle, we need its children
      if (nodeList.getLength() == 1) {
        nodeList = nodeList.item(0).getChildNodes();
      }
      return nodeList;
    }
    return null;
  }
}
