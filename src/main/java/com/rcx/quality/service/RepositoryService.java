package com.rcx.quality.service;

import com.rcx.quality.db.ProjectQualityStorage;
import com.rcx.quality.db.Repository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositoryService {

  @Autowired
  Repository repository;

  public void storeRecord(final String group,
                          final String name,
                          final int info,
                          final int warning,
                          final int error,
                          final double coverage,
                          final long when) {
    ProjectQualityStorage storage = new ProjectQualityStorage();
    storage.setProjectGroup(group);
    storage.setProjectName(name);
    storage.setInfoIssues(info);
    storage.setWarningIssues(warning);
    storage.setErrorIssues(error);
    storage.setCoverage(coverage);
    storage.setMeasureTime(when);
    repository.save(storage);
  }

  public List<ProjectQualityStorage> getRecords(final String group) {
    return repository.findAllByProjectGroupOrderByMeasureTimeAsc(group);
  }

  public Map<String, List<ProjectQualityStorage>> getRecordsPerProject(
      final String group,
      final String timeLimit) {
    return extractPerProject(repository.findAllByProjectGroupOrderByMeasureTimeAsc(group),
        timeLimit);
  }

  public Map<String, List<ProjectQualityStorage>> extractPerProject(
      final List<ProjectQualityStorage> history,
      final String timeLimit) {

    Calendar limit = null;
    if (timeLimit != null) {
      limit = Calendar.getInstance();
      try {
        if (timeLimit.trim().toUpperCase().endsWith("W")) {
          int weeks = Integer.parseInt(timeLimit.toUpperCase().replaceAll("W", ""));
          limit.add(Calendar.DAY_OF_YEAR, -weeks);
        } else if (timeLimit.trim().toUpperCase().endsWith("M")) {
          int months = Integer.parseInt(timeLimit.toUpperCase().replaceAll("M", ""));
          limit.add(Calendar.MONTH, -months);
        } else {
          // invalid limit, ignore it
          limit = null;
        }
      } catch (final NumberFormatException e) {
        // invalid limit
        limit = null;
      }

    }

    final Map<String, List<ProjectQualityStorage>> result = new HashMap<>();
    List<ProjectQualityStorage> list;
    final Date now = new Date();
    for (final ProjectQualityStorage storage : history) {
      if (result.containsKey(storage.getProjectName())) {
        list = result.get(storage.getProjectName());
      } else {
        list = new ArrayList<>();
        result.put(storage.getProjectName(), list);
      }
      if (limit == null || limit.before(now)) {
        list.add(storage);
      }
    }
    return result;
  }


}
