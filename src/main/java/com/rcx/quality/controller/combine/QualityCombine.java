package com.rcx.quality.controller.combine;

import com.rcx.quality.model.QualityLineIssue;
import com.rcx.quality.model.QualityResult;
import com.rcx.quality.model.QualitySeverity;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class QualityCombine {

  private QualityCombine() {

  }

  public static Map<QualitySeverity, Integer> combineResults(final List<QualityResult> qualityResults) {
    QualityResult[] results = new QualityResult[qualityResults.size()];
    return combineResults(qualityResults.toArray(results));
  }


  public static Map<QualitySeverity, Integer> combineResults(final QualityResult... qualityStatus) {
    final EnumMap<QualitySeverity, Integer> result = new EnumMap<>(QualitySeverity.class);
    result.put(QualitySeverity.ERROR, 0);
    result.put(QualitySeverity.INFO, 0);
    result.put(QualitySeverity.WARNING, 0);
    if (qualityStatus != null && qualityStatus.length > 0) {
      for (final QualityResult qualityResult : qualityStatus) {
        for (final List<QualityLineIssue> issues : qualityResult.getResults().values()) {
          populateMap(result, issues);
        }
      }
    }

    return result;
  }

  private static void populateMap(final EnumMap<QualitySeverity, Integer> severities, final List<QualityLineIssue> issues) {
    for (final QualityLineIssue issue : issues) {
      if (severities.containsKey(issue.getSeverity())) {
        severities.put(issue.getSeverity(), severities.get(issue.getSeverity()) + 1);
      } else {
        severities.put(issue.getSeverity(), 1);
      }
    }
  }

}
