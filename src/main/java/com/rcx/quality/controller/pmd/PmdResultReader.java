package com.rcx.quality.controller.pmd;

import com.rcx.quality.controller.QualityReader;
import com.rcx.quality.model.QualityLineIssue;
import com.rcx.quality.model.QualityResult;
import com.rcx.quality.model.QualitySeverity;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PmdResultReader extends QualityReader {

  private PmdResultReader() {

  }

  public static QualityResult loadPmd(final String contents) {
    final QualityResult result = new QualityResult();

    final NodeList nodeList = loadChildNodes(contents);
    for (int i = 0; i < nodeList.getLength(); i++) {
      final Node node = nodeList.item(i);


      if ("file".equals(node.getNodeName()) && node.getChildNodes() != null &&
          node.getChildNodes().getLength() > 0) {
        // This file has issues
        final NodeList fileIssues = node.getChildNodes();
        final Node filenameNode = node.getAttributes().getNamedItem("name");
        if (filenameNode != null) {
          final String filename = filenameNode.getNodeValue();
          final List<QualityLineIssue> issues = new ArrayList<>();
          for (int issue = 0; issue < fileIssues.getLength(); issue++) {
            final Node issueNode = fileIssues.item(issue);

            int lineNumber = Integer
                .parseInt(issueNode.getAttributes().getNamedItem("beginline").getNodeValue());
            final int severity =
                Integer.parseInt(issueNode.getAttributes().getNamedItem("priority").getNodeValue());
            QualitySeverity qualitySeverity;
            if (severity == 3) {
              qualitySeverity = QualitySeverity.WARNING;
            } else if (severity < 3) {
              qualitySeverity = QualitySeverity.ERROR;
            } else {
              qualitySeverity = QualitySeverity.INFO;
            }
            final String message = issueNode.getTextContent();
            final String source =
                issueNode.getAttributes().getNamedItem("ruleset").getNodeValue() + "/" +
                    issueNode.getAttributes().getNamedItem("rule").getNodeValue();
            issues.add(new QualityLineIssue(lineNumber, source, message, qualitySeverity));
          }
          result.getResults().put(filename, issues);
        }
      }
    }

    return result;
  }

}
