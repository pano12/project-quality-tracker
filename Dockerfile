FROM openjdk:8-jdk-alpine
VOLUME /tmp
#HEALTHCHECK --interval=12s --timeout=12s --start-period=30s \
#    CMD curl --fail http://localhost:8080/health || exit 1
ARG DEPENDENCY=build/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-cp","app:app/lib/*","com.rcx.quality.Main"]
