package com.rcx.quality.controller.jacoco;

import com.rcx.quality.db.ProjectQualityStorage;
import com.rcx.quality.model.JacocoCoverageType;
import com.rcx.quality.tools.StringTools;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class JacocoResultReaderTest {

  @Test
  public void testGetLineCoverage() throws IOException {
    ProjectQualityStorage test = new ProjectQualityStorage();

    final String jacocoFile =
        StringTools.convert(getClass().getResourceAsStream("/jacoco/main.xml"));
    double resultBranch = JacocoResultReader.getLineCoverage(jacocoFile, JacocoCoverageType.BRANCH);
    double resultClass = JacocoResultReader.getLineCoverage(jacocoFile, JacocoCoverageType.CLASS);
    double resultComplexity =
        JacocoResultReader.getLineCoverage(jacocoFile, JacocoCoverageType.COMPLEXITY);
    double resultInstruction =
        JacocoResultReader.getLineCoverage(jacocoFile, JacocoCoverageType.INSTRUCTION);
    double resultLine = JacocoResultReader.getLineCoverage(jacocoFile, JacocoCoverageType.LINE);
    double resultMethod = JacocoResultReader.getLineCoverage(jacocoFile, JacocoCoverageType.METHOD);

    Assert.assertEquals("Branch coverage", 0.407d, resultBranch, 1e-3);
    Assert.assertEquals("Class coverage", 0.666d, resultClass, 1e-3);
    Assert.assertEquals("Complexity coverage", 0.324d, resultComplexity, 1e-3);
    Assert.assertEquals("Instruction coverage", 0.643d, resultInstruction, 1e-3);
    Assert.assertEquals("Line coverage", 0.609d, resultLine, 1e-3);
    Assert.assertEquals("Method coverage", 0.5d, resultMethod, 1e-3);

  }
}