# This project aims at regrouping quality measurements over a group of projects

Ever had to deliver quality measures for a software project, split into several functionalities?

This will help you manage clear pointers. Call this, feed it with your checkstyle, PMD, SpotBugs, Jacoco XML outputs.

Their quality issues (Info, Warning, Error) will be displayed in a single graph.
All tools, all software features (Git projects), in a single one, to supply to your clients, or to yourself.

This tool's goal is merely a summary of quality results. No processing capabilities over it, as you should already be tracking them with the appropriate tools :-)

The project is distributed through a Docker image, to run, and conserve its volume over time, to keep a history of past runs, in order to supply a timeline for all executions.
A single instance can feed several groups of projects.

For example, say you have a microservices architecture, called MyDistributedProject, composed of 5 microservices (A, B, C, D, E).
Following quality of projects A, B, C, D, E will be done on the same graph, for all 5 of them.

Now, someone else calls the same Docker, same volume, with a MyOtherDistributedProject, and has similar names for the subprojects (A, B, C), he'll only have graphs matching his MyOtherDistributedProject group.

Output Chart example: 
![alt text](doc/chart.png "Example chart with only 1 project")


## Todo :
- [ ] Create Coverage graph
- [x] Replace CanvasJS with Open Source charts
- [ ] Sonarqube plugin to inject measurements / OWASP dashboard
- [ ] Security dashboard
