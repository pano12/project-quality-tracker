package com.rcx.quality.controller.output;

import com.rcx.quality.db.ProjectQualityStorage;
import com.rcx.quality.service.RepositoryService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

public class HTMLQualityHistoryReportTest {

  private List<ProjectQualityStorage> generateTestSet() {
    final List<ProjectQualityStorage> input = new ArrayList<>();
    final String group = "Group";
    final String[] projects = new String[] {"µService 1", "Service 2", "Gateway", "Kafka"};

    Calendar timeLine = Calendar.getInstance();

    ProjectQualityStorage value = new ProjectQualityStorage();
    timeLine.set(2018, Calendar.DECEMBER, 10, 8, 15, 20);
    value.setCoverage(0.4);
    value.setErrorIssues(3);
    value.setWarningIssues(15);
    value.setInfoIssues(10);
    value.setProjectGroup(group);
    value.setProjectName(projects[0]);
    value.setMeasureTime(timeLine.getTimeInMillis());
    value.setId(1L);

    input.add(value);

    value = new ProjectQualityStorage();
    timeLine.set(2018, Calendar.DECEMBER, 11, 10, 10, 10);
    value.setCoverage(0.6);
    value.setErrorIssues(2);
    value.setWarningIssues(10);
    value.setInfoIssues(1);
    value.setProjectGroup(group);
    value.setProjectName(projects[0]);
    value.setMeasureTime(timeLine.getTimeInMillis());
    value.setId(2L);

    input.add(value);
    value = new ProjectQualityStorage();
    timeLine.set(2018, Calendar.DECEMBER, 11, 18, 15, 20);
    value.setCoverage(0.7);
    value.setErrorIssues(5);
    value.setWarningIssues(1);
    value.setInfoIssues(2);
    value.setProjectGroup(group);
    value.setProjectName(projects[0]);
    value.setMeasureTime(timeLine.getTimeInMillis());
    value.setId(3L);

    input.add(value);
    value = new ProjectQualityStorage();
    timeLine.set(2018, Calendar.DECEMBER, 12, 8, 50, 20);
    value.setCoverage(0.5);
    value.setErrorIssues(1);
    value.setWarningIssues(1);
    value.setInfoIssues(2);
    value.setProjectGroup(group);
    value.setProjectName(projects[0]);
    value.setMeasureTime(timeLine.getTimeInMillis());
    value.setId(4L);

    input.add(value);


    value = new ProjectQualityStorage();
    timeLine.set(2018, Calendar.DECEMBER, 9, 17, 45, 20);
    value.setCoverage(0.1);
    value.setErrorIssues(5);
    value.setWarningIssues(4);
    value.setInfoIssues(2);
    value.setProjectGroup(group);
    value.setProjectName(projects[1]);
    value.setMeasureTime(timeLine.getTimeInMillis());
    value.setId(5L);

    input.add(value);

    value = new ProjectQualityStorage();
    timeLine.set(2018, Calendar.DECEMBER, 10, 10, 10, 10);
    value.setCoverage(0.8);
    value.setErrorIssues(2);
    value.setWarningIssues(14);
    value.setInfoIssues(11);
    value.setProjectGroup(group);
    value.setProjectName(projects[1]);
    value.setMeasureTime(timeLine.getTimeInMillis());
    value.setId(6L);

    input.add(value);
    value = new ProjectQualityStorage();
    timeLine.set(2018, Calendar.DECEMBER, 13, 18, 15, 20);
    value.setCoverage(0.9);
    value.setErrorIssues(1);
    value.setWarningIssues(0);
    value.setInfoIssues(0);
    value.setProjectGroup(group);
    value.setProjectName(projects[1]);
    value.setMeasureTime(timeLine.getTimeInMillis());
    value.setId(7L);

    input.add(value);
    return input;
  }

  @Test
  public void testGenerateHTMLReport() {
    // Test actual output

    System.out.println(HTMLQualityHistoryReport.generateHTMLReport(
        "Pano",
        new RepositoryService().extractPerProject(generateTestSet(), null)));

  }
}