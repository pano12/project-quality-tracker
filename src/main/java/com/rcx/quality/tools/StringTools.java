package com.rcx.quality.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class StringTools {

  /**
   * Private constructor.
   */
  private StringTools() {
  }

  public static String convert(final InputStream inputStream) throws IOException {
    String line = null;

    StringBuilder builder = new StringBuilder();
    try (final BufferedReader bufferedReader = new BufferedReader(
        new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
      while ((line = bufferedReader.readLine()) != null) {
        builder.append(line);
      }
    }

    return builder.toString();
  }
}
