package com.rcx.quality.model;

public enum QualitySeverity {
  INFO,
  WARNING,
  ERROR
}
