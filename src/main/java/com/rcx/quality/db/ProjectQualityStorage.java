package com.rcx.quality.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class ProjectQualityStorage {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private long measureTime;
  private int infoIssues;
  private int warningIssues;
  private int errorIssues;
  private double coverage;
  private String projectName;
  private String projectGroup;

}
