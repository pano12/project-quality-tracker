package com.rcx.quality.model;

public enum JacocoCoverageType {
  INSTRUCTION,
  BRANCH,
  LINE,
  COMPLEXITY,
  METHOD,
  CLASS
}
