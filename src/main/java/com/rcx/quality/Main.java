package com.rcx.quality;

import com.rcx.quality.controller.checkstyle.CheckstyleResultReader;
import com.rcx.quality.controller.combine.QualityCombine;
import com.rcx.quality.controller.jacoco.JacocoResultReader;
import com.rcx.quality.controller.output.HTMLCoverageHistoryReport;
import com.rcx.quality.controller.output.HTMLQualityHistoryReport;
import com.rcx.quality.controller.output.HTMLQualityStatusReport;
import com.rcx.quality.controller.pmd.PmdResultReader;
import com.rcx.quality.controller.spotbugs.SpotbugsResultReader;
import com.rcx.quality.db.ProjectQualityStorage;
import com.rcx.quality.model.JacocoCoverageType;
import com.rcx.quality.model.QualityResult;
import com.rcx.quality.model.QualitySeverity;
import com.rcx.quality.service.RepositoryService;
import com.rcx.quality.tools.StringTools;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {

  private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

  public static void main(final String[] args) {
    SpringApplication.run(Main.class, args);
  }

  private static String extractParameter(final String parameter) {
    return parameter.substring(parameter.indexOf('=') + 1);
  }

  private static void usage() {
    System.out.println("== Usage ==");
    System.out.println("-c=, --checkstyle= : Checkstyle results");
    System.out.println("-g=, --group= : Project group name [MANDATORY]");
    System.out.println("-i, --info: Only generate output, no input given");
    System.out.println("-j=, --jacoco= : JACOCO results");
    System.out.println(
        "-l=, --limit= : Limit results in the past [weeks or months, syntax: 3m = 3 months, 1w = 1 week]");
    System.out.println("-n=, --name= : Project name [MANDATORY]");
    System.out.println("-o=, --output= : Output folder [default value: /data/output]");
    System.out.println("-p=, --pmd= : PMD results");
    System.out.println("-s=, --spotbugs= : SpotBugs results");
    System.out.println("-w=, --when= : Dataset time (epoch) [default value: now]");
  }


  @Bean
  public CommandLineRunner commandLineRunner(final ApplicationContext ctx) {
    return new CommandLineRunner() {

      @Autowired
      RepositoryService repositoryService;

      @Override
      public void run(String... args) throws Exception {
        if (args != null && args.length > 0) {
          QualityResult r;
          String projectName = null;
          String projectGroup = null;
          Long date = null;
          String outputFolder = "/output";
          boolean outputOnly = false;
          String limit = null;

          final List<QualityResult> qualityResults = new ArrayList<>();
          double coverage = 0.0;
          String file;
          for (final String arg : args) {
            if (arg.startsWith("-c=") || arg.startsWith("--checkstyle=")) {
              try {
                file = StringTools.convert(new FileInputStream(extractParameter(arg)));
                r = CheckstyleResultReader.loadCheckstyle(file);
                if (r != null) {
                  qualityResults.add(r);
                }
              } catch (final FileNotFoundException e) {
                LOGGER.error("Unable to locate Checkstyle file");
              } catch (final IOException e) {
                LOGGER.error("Unable to read Checkstyle file");
              }
            } else if (arg.equals("-i") || arg.equals("--info")) {
              outputOnly = true;
            } else if (arg.startsWith("-j=") || arg.startsWith("--jacoco=")) {
              try {
                file = StringTools.convert(new FileInputStream(extractParameter(arg)));
                coverage = JacocoResultReader.getLineCoverage(file, JacocoCoverageType.INSTRUCTION);
              } catch (final FileNotFoundException e) {
                LOGGER.error("Unable to locate Jacoco file");
              } catch (final IOException e) {
                LOGGER.error("Unable to read Jacoco file");
              }
            } else if (arg.startsWith("-l=") || arg.startsWith("--limit=")) {
              limit = extractParameter(arg);
            } else if (arg.startsWith("-p=") || arg.startsWith("--pmd=")) {
              try {
                file = StringTools.convert(new FileInputStream(extractParameter(arg)));
                r = PmdResultReader.loadPmd(file);
                if (r != null) {
                  qualityResults.add(r);
                }
              } catch (final FileNotFoundException e) {
                LOGGER.error("Unable to locate PMD file");
              } catch (final IOException e) {
                LOGGER.error("Unable to read PMD file");
              }
            } else if (arg.startsWith("-s=") || arg.startsWith("--spotbugs=")) {
              try {
                file = StringTools.convert(new FileInputStream(extractParameter(arg)));
                r = SpotbugsResultReader.loadSpotBugs(file);
                if (r != null) {
                  qualityResults.add(r);
                }
              } catch (final FileNotFoundException e) {
                LOGGER.error("Unable to locate SpotBugs file");
              } catch (final IOException e) {
                LOGGER.error("Unable to read SpotBugs file");
              }
            } else if (arg.startsWith("-n=") || arg.startsWith("--name=")) {
              projectName = extractParameter(arg);
            } else if (arg.startsWith("-g=") || arg.startsWith("--group=")) {
              projectGroup = extractParameter(arg);
            } else if (arg.startsWith("-w=") || arg.startsWith("--when=")) {
              date = Long.parseLong(extractParameter(arg));
            } else if (arg.startsWith("-o=") || arg.startsWith("--output=")) {
              outputFolder = extractParameter(arg);
            }
          }

          if (projectGroup == null || projectName == null) {
            System.out.println("ERROR: No project name nor group defined, they're mandatory");
            System.out.println();
            usage();
            System.exit(1);
          }
          if (!outputOnly) {
            final Map<QualitySeverity, Integer> combinedQuality =
                QualityCombine.combineResults(qualityResults);
            repositoryService.storeRecord(
                projectGroup,
                projectName,
                combinedQuality.get(QualitySeverity.INFO),
                combinedQuality.get(QualitySeverity.WARNING),
                combinedQuality.get(QualitySeverity.ERROR),
                coverage,
                date == null ? new Date().getTime() : date
            );
          }
          final File qualityDashboard = new File(outputFolder + "/quality_dashboard.html");
          final File coverageDashboard = new File(outputFolder + "/coverage_dashboard.html");
          final File qualitySummary = new File(outputFolder + "/quality_summary.html");

          final File parentFolder = new File(outputFolder);

          if (parentFolder.exists() || parentFolder.mkdirs()) {

            final Map<String, List<ProjectQualityStorage>> dataInput =
                repositoryService.getRecordsPerProject(projectGroup, limit);



            PrintWriter pw = new PrintWriter(qualityDashboard);
            pw.println(HTMLQualityHistoryReport.generateHTMLReport(projectGroup, dataInput));
            pw.close();

            pw = new PrintWriter(coverageDashboard);
            pw.println(HTMLCoverageHistoryReport.generateHTMLReport(projectGroup, dataInput));
            pw.close();

            pw = new PrintWriter(qualitySummary);
            pw.println(HTMLQualityStatusReport.generateHTMLReport(projectGroup, dataInput));
            pw.close();

          } else {
            System.out.println("ERROR: Unable to create output folder");
            System.out.println();
            usage();
            System.exit(1);
          }
        } else {
          usage();
        }

      }
    };
  }

}
