package com.rcx.quality.controller.checkstyle;

import com.rcx.quality.model.QualityResult;
import com.rcx.quality.tools.StringTools;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class CheckstyleResultReaderTest {

  @Test
  public void testLoadCheckstyle() throws IOException {
    final String checkstyleFile = StringTools.convert(getClass().getResourceAsStream("/checkstyle/main.xml"));
    final QualityResult result = CheckstyleResultReader.loadCheckstyle(checkstyleFile);
    assertNotNull("Checkstyle loaded shouldn't be null", result);
    assertNotNull("Checkstyle results shouldn't be null", result.getResults());
    assertFalse("There is existing errors", result.getResults().isEmpty());
    assertEquals("There is 2 files with errors", 2, result.getResults().size());
  }
}