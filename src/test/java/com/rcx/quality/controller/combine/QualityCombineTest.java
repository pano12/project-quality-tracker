package com.rcx.quality.controller.combine;

import com.rcx.quality.controller.checkstyle.CheckstyleResultReader;
import com.rcx.quality.controller.pmd.PmdResultReader;
import com.rcx.quality.controller.spotbugs.SpotbugsResultReader;
import com.rcx.quality.model.QualityResult;
import com.rcx.quality.model.QualitySeverity;
import com.rcx.quality.tools.StringTools;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

public class QualityCombineTest {

  @Test
  public void testCombineResults() throws IOException {

    final String checkstyleFile = StringTools.convert(getClass().getResourceAsStream("/checkstyle/main.xml"));
    final QualityResult checkstyle = CheckstyleResultReader.loadCheckstyle(checkstyleFile);

    final String pmdFile = StringTools.convert(getClass().getResourceAsStream("/pmd/main.xml"));
    final QualityResult pmd = PmdResultReader.loadPmd(pmdFile);

    final String spotbugsFile = StringTools.convert(getClass().getResourceAsStream("/spotbugs/main.xml"));
    final QualityResult spotbugs = SpotbugsResultReader.loadSpotBugs(spotbugsFile);

    final Map<QualitySeverity, Integer> result = QualityCombine.combineResults(checkstyle, pmd, spotbugs);

    Assert.assertNotNull("There's supposed to be errors", result);
    Assert.assertNotNull("ERROR severity has a value filled in", result.get(QualitySeverity.ERROR));
    Assert.assertNotNull("INFO severity has a value filled in", result.get(QualitySeverity.INFO));
    Assert.assertNotNull("WARNING severity has a value filled in", result.get(QualitySeverity.WARNING));
    Assert.assertEquals("There is no ERROR severity", 0, result.get(QualitySeverity.ERROR).intValue());
    Assert.assertEquals("There is no INFO severity", 0, result.get(QualitySeverity.INFO).intValue());
    Assert.assertEquals("There is 16 WARNING severity", 16, result.get(QualitySeverity.WARNING).intValue());
  }
}
